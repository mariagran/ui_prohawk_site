(function(){

	$(document).ready(function(){

		/* 
		** Carrossel de serviços
		*/
		$('.carrossel-servicos').owlCarousel({
			items: 1,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			smartSpeed: 450,
		});

		/* 
		** Função serviços: mostrar conteúdo de cada produto no clique
		** início
		*/

		/*-- desktop e mobile: mostra o conteúdo de cada serviço --*/
		let liProdutos = document.querySelectorAll('.pg-inicial .secao-servicos .produtos-gerados ul li');
		let produtos = document.querySelectorAll('.pg-inicial .secao-servicos .conteudo-produtos-gerados');

		liProdutos.forEach(function(item){
			item.addEventListener('click', function(){
				let dataId = this.getAttribute('data-id');

				liProdutos.forEach(function(item){
					item.classList.remove('produto-ativo');
				});
				this.classList.add('produto-ativo');

				produtos.forEach(function(item){
					item.classList.remove('conteudo-produtos-ativo');
				});

				let produto = produtos[dataId];
				produto.classList.add('conteudo-produtos-ativo');

				if(screen.width <= 768){
					ulProdutos.classList.remove('abrir-menu-produtos-gerados');
					isMenuProdutosOpen = false;
					spanProdutoAtivo.innerText = item.innerText;
				}
			});
		});
		document.querySelector('.pg-inicial .secao-servicos .produtos-gerados ul li[data-id="0"]').click();
		
		/*-- mobile: abre e fecha o menu mobile --*/
		let spanProdutoAtivo = document.querySelector('.pg-inicial .secao-servicos .produtos-gerados .menu-produtos-gerados .span-produto-ativo');
		let ulProdutos = document.querySelector('.pg-inicial .secao-servicos .produtos-gerados .menu-produtos-gerados ul');

		let isMenuProdutosOpen = false;

		spanProdutoAtivo.addEventListener('click', function(){
			if(!isMenuProdutosOpen){
				ulProdutos.classList.add('abrir-menu-produtos-gerados');
				isMenuProdutosOpen = true;
			} else{
				ulProdutos.classList.remove('abrir-menu-produtos-gerados');
				isMenuProdutosOpen = false;
			}
		});
		/* 
		** Função serviços
		** fim
		*/
		
		/* 
		** Função áreas de atuação: abrir ou fechar formulário de contato
		** início
		*/
		let contentFormulario = document.getElementById('formularioFaleConosco');
		contentFormulario.remove();
		$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li .span-arrow img').click(function(){
			if(screen.width > 500){
				let thisLi = $(this).parent().parent();

				$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li').removeClass('active-li');
				thisLi.addClass('active-li');

				$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li .container-formulario').removeClass('open-formulario');

				let containerFormulario = thisLi.children('.container-formulario');
				containerFormulario.append(contentFormulario);

				containerFormulario.addClass('open-formulario');
				setTimeout(function(){
					contentFormulario.classList.add('show-formulario');
				}, 250);
			}
		});

		$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li').mouseover(function(){
			if(screen.width > 500){
				$('#formularioFaleConosco .fechar-formulario img').click(function(){
					let containerFormulario = $(this).parent().parent().parent();

					contentFormulario.classList.remove('show-formulario');
					setTimeout(function(){
						containerFormulario.removeClass('open-formulario');
						$('.pg-inicial .secao-areas-atuacao article .areas-atuacao li').removeClass('active-li');
						contentFormulario.remove();
					}, 250);

				});
			}
		});
		/* 
		** Função áreas de atuação: abrir ou fechar formulário de contato
		** fim
		*/

		$('.scrollTop a').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});

		/* 
		** Funções de vídeo
		** início
		*/
		let video = document.querySelector('.pg-inicial .secao-destaque video');

		let playButton = document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .div-icones-video .icone-play-video img');
		let pauseButton = document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .div-icones-video .icone-pause-video img');
		let muteButton = document.querySelector('.pg-inicial .secao-destaque .conteudo-destaque article .div-icones-video .icone-mute-video img');

		playButton.addEventListener('click', function(){
			video.play();
			playButton.src = 'img/playb.svg';
			pauseButton.src = 'img/pausew.svg';
		});
		pauseButton.addEventListener('click', function(){
			video.pause();
			pauseButton.src = 'img/pauseb.svg';
			playButton.src = 'img/playw.svg';
		});
		muteButton.addEventListener('click', function(){
			if(video.muted){
				video.muted = false;
				muteButton.src = 'img/mute.svg';
			} else{
				video.muted = true;
				muteButton.src = 'img/sound.svg';
			}
		});
		/* 
		** Funções de vídeo
		** fim
		*/

		

	});

}());